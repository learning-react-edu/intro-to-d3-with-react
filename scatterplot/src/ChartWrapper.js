import { useEffect, useRef, useState } from 'react';
import { D3Chart } from './D3Chart';

const ChartWrapper = ({ data, updateName }) => {
  const chartArea = useRef(null);
  const [ chart, setChart ] = useState(null);

  useEffect(() => {
    if (!chart) {
      setChart(new D3Chart(chartArea.current, data, updateName));
    } else {
      chart.update(data);
    }
  }, [chartArea, chart, data, updateName]);

  return (
    <div className='chart-area' ref={chartArea}></div>
  );
};

export {
  ChartWrapper,
};
