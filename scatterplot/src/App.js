import { useEffect, useState } from 'react';
import { json } from 'd3';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Row from 'react-bootstrap/Row';
import { ChartWrapper } from './ChartWrapper';
import { Table } from './Table';

function App() {
  const [ data, setData ] = useState([]);
  const [ activeName, setActiveName ] = useState('');
  const url = 'https://udemy-react-d3.firebaseio.com/children.json';

  useEffect(() => {
    json(url)
    .then(res => setData(res))
    .catch(error => console.log(error));
  }, [url]);

  const renderChart = () => {
    if (data.length === 0) {
      return 'No data yet';
    } else {
      return (<ChartWrapper data={data} updateName={setActiveName} />);
    }
  };

  return (
    <>
      <Navbar bg="light">
        <Container>
          <Navbar.Brand>Scatterplotly</Navbar.Brand>
        </Container>
      </Navbar>
      <Container>
        <Row>
          <Col md={6} xs={12}>{renderChart()}</Col>
          <Col md={6} xs={12}><Table data={data} updateData={setData} activeName={activeName} /></Col>
        </Row>
      </Container>
    </>
  );
}

export default App;
