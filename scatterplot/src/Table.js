import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { Button } from 'react-bootstrap';
import { useState } from 'react';

const Table = ({ data, activeName, updateData }) => {
  const [ nameInput, setNameInput ] = useState('');
  const [ heightInput, setHeightInput ] = useState('');
  const [ ageInput, setAgeInput ] = useState('');

  const handleRemove = (event) => {
    updateData(data.filter(d => d.name !== event.target.name));
  };

  const handleAdd = () => {
    updateData([
      ...data,
      {
        name: nameInput,
        height: heightInput,
        age: ageInput,
      },
    ]);
    setNameInput('');
    setHeightInput('');
    setAgeInput('');
  }

  return (
    <>
      <Row>
        <Col xs={3}>
          <Form.Control
            placeholder='Name'
            name={'name'}
            value={nameInput}
            onChange={e => setNameInput(e.target.value)}
          />
        </Col>
        <Col xs={3}>
          <Form.Control
            placeholder='Height'
            name={'height'}
            value={heightInput}
            onChange={e => setHeightInput(e.target.value)}
          />
        </Col>
        <Col xs={3}>
          <Form.Control
            placeholder='Age'
            name={'age'}
            value={ageInput}
            onChange={e => setAgeInput(e.target.value)}
          />
        </Col>
        <Col>
          <Button
            variant='primary'
            type='button'
            style={{ width: '100%' }}
            onClick={handleAdd}
          >
            Add
          </Button>
        </Col>
      </Row>
      {data && data.length > 0 ?
        data.map(student => (
          <Row
            key={student.name}
            style={{ marginTop: '10px', backgroundColor: student.name === activeName ? 'grey': 'white' }}
          >
            <Col xs={3}>{student.name}</Col>
            <Col xs={3}>{student.height}</Col>
            <Col xs={3}>{student.age}</Col>
            <Col xs={3}>
              <Button
                variant='danger'
                type='button'
                style={{ width: '100%' }}
                name={student.name}
                onClick={handleRemove}
              >
                Remove
              </Button>
            </Col>
          </Row>
        )) : null}
    </>
  );
};

export {
  Table,
};
