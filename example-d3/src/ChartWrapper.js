import React, { useEffect, useRef } from 'react';
import { D3Chart } from "./D3Chart";

export const ChartWrapper = () => {
    const chart = useRef(null);

    useEffect(() => {
        new D3Chart(chart.current);
    }, [chart]);

    return (
        <div ref={chart} />
    );
};
