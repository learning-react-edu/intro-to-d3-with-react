import React from 'react';
import { Dropdown } from "react-bootstrap";
import { Gender } from './App';

export const GenderDropdown = ({ setGender }) => (
  <Dropdown onSelect={setGender}>
    <Dropdown.Toggle variant='primary' id='dropdown-basic'>
      Please select gender
    </Dropdown.Toggle>

    <Dropdown.Menu>
      <Dropdown.Item eventKey={Gender.MEN}>Men</Dropdown.Item>
      <Dropdown.Item eventKey={Gender.WOMEN}>Women</Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
