import { useEffect, useRef, useState } from "react";
import { D3Chart } from "./D3Chart";

export const ChartWrapper = ({ gender }) => {
  const chart = useRef(null);
  const [ d3Chart, setD3Chart ] = useState(null);

  useEffect(() => {
    setD3Chart(new D3Chart(chart.current));
  }, [chart]);

  useEffect(() => {
    if (d3Chart && gender) {
      d3Chart.update(gender);
    }
  }, [gender]);

  return (
    <div ref={chart} />
  );
};
