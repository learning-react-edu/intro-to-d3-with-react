import { ChartWrapper } from "./ChartWrapper";
import { Col, Container, Navbar, Row } from "react-bootstrap";
import { GenderDropdown } from "./GenderDropdown";
import { useState } from "react";

export const Gender = Object.freeze({
  MEN: 'men',
  WOMEN: 'women',
});

function App() {
  const [ gender, setGender ] = useState(Gender.MEN);

  return (
    <div className="App">
      <Navbar bg="light">
        <Navbar.Brand>Barchartly</Navbar.Brand>
      </Navbar>
      <Container>
        <Row>
          <Col xs={12}><GenderDropdown setGender={setGender}/></Col>
        </Row>
        <Row>
          <Col xs={12}><ChartWrapper gender={gender}/></Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
